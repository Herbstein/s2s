use byteorder::{BigEndian, ReadBytesExt};
use std::ffi::CString;
use std::{io::Read, str::from_utf8, string::FromUtf8Error};

const INIT_MSG_TYPE: u8 = 0x2d;
const INIT_MSG_SIZE: usize = 400;
const INIT_MSG_IDENT_LENGTH: usize = 25;
const INIT_MSG_IDENT_OFFSET: usize = 0;
const INIT_MSG_SERVER_NAME_LENGTH: usize = 256;
const INIT_MSG_SERVER_NAME_OFFSET: usize = 128;
const INIT_MSG_API_PORT_LENGTH: usize = 5;
const INIT_MSG_API_PORT_OFFSET: usize = 384;

type Error = Box<dyn std::error::Error>;

#[derive(Debug)]
pub enum Message {
    InitMsg(InitMsg),
    CtrlMsg(CtrlMsg),
}

pub struct Decoder<R> {
    reader: R,
    buffer: Vec<u8>,
}

impl<R: Read> Decoder<R> {
    pub fn new(r: R) -> Decoder<R> {
        Decoder {
            reader: r,
            buffer: Vec::new(),
        }
    }

    pub fn decode(&mut self) -> Result<Message, Error> {
        let mut data = vec![0, 0, 0, 0];
        let n = self.reader.read(&mut data)?;

        self.buffer.append(&mut data.to_vec());
        if n != 4 {
            return Err("4 bytes not available".into());
        }

        match *data.as_slice() {
            [INIT_MSG_TYPE, ..] => {
                while self.buffer.len() < INIT_MSG_SIZE {
                    let buf_size = INIT_MSG_SIZE - data.len();
                    data = vec![0; buf_size];
                    self.reader.read_exact(&mut data)?;
                    self.buffer.append(&mut data);
                }

                let ret = InitMsg::decode(&self.buffer[..INIT_MSG_SIZE])?;
                self.buffer = self.buffer[INIT_MSG_SIZE..].to_vec();
                Ok(Message::InitMsg(ret))
            }
            _ => {
                if self.buffer.len() < 4 {
                    return Err("unknown type".into());
                }

                let msg_size = (&data[0..4]).read_u32::<BigEndian>()? as usize + 4;
                while self.buffer.len() < msg_size {
                    data = vec![0; msg_size - data.len()];
                    self.reader.read_exact(&mut data)?;
                    self.buffer.append(&mut data);
                }

                let ret = CtrlMsg::decode(&self.buffer[..msg_size])?;
                self.buffer = self.buffer[msg_size..].to_vec();
                Ok(Message::CtrlMsg(ret))
            }
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct InitMsg {
    pub identifier: String,
    pub servername: String,
    pub api_port: u16,
}

impl InitMsg {
    fn decode(data: &[u8]) -> Result<InitMsg, Error> {
        let input_length = data.len();
        if input_length != INIT_MSG_SIZE {
            return Err("Not enough data given".into());
        }

        if from_utf8(&data[..INIT_MSG_IDENT_LENGTH])? != "--splunk-cooked-mode-v3--" {
            return Err("First 25 bytes not as expected".into());
        }

        let identifier =
            to_string(&data[INIT_MSG_IDENT_OFFSET..INIT_MSG_IDENT_OFFSET + INIT_MSG_IDENT_LENGTH])?;

        let servername = to_string(
            &data[INIT_MSG_SERVER_NAME_OFFSET
                ..INIT_MSG_SERVER_NAME_OFFSET + INIT_MSG_SERVER_NAME_LENGTH],
        )?;

        let api_port = to_string(
            &data[INIT_MSG_API_PORT_OFFSET..INIT_MSG_API_PORT_OFFSET + INIT_MSG_API_PORT_LENGTH],
        )?;
        let api_port = api_port.parse::<u16>()?;

        Ok(InitMsg {
            identifier,
            servername,
            api_port,
        })
    }

    pub fn encode(&self) -> Result<Vec<u8>, Error> {
        let id = self.identifier.as_bytes();
        let s = self.servername.as_bytes();
        let a = self.api_port.to_string();
        let a = a.as_bytes();

        if id.len() != INIT_MSG_IDENT_LENGTH {
            return Err("Indentifier malformed".into());
        }

        if s.len() > INIT_MSG_SERVER_NAME_LENGTH {
            return Err("Servername too long".into());
        }

        if a.len() > INIT_MSG_API_PORT_LENGTH {
            return Err("API Port out of range".into());
        }

        let ret = [
            pad_to_size(id, INIT_MSG_SERVER_NAME_OFFSET - INIT_MSG_IDENT_OFFSET),
            pad_to_size(s, INIT_MSG_API_PORT_OFFSET - INIT_MSG_SERVER_NAME_OFFSET),
            pad_to_size(a, INIT_MSG_SIZE - INIT_MSG_API_PORT_OFFSET),
        ];

        let ret = ret.concat();

        Ok(ret)
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct CtrlMsg {
    pub typ: u32,
    pub key: String,
    pub value: String,
    pub trailer: String,
}

impl CtrlMsg {
    fn decode(data: &[u8]) -> Result<CtrlMsg, Error> {
        let ctrl_msg_type = (&data[4..8]).read_u32::<BigEndian>()?;
        let key_size = (&data[8..12]).read_u32::<BigEndian>()? as usize;
        let key_offset = 12;
        let value_offset = (key_offset + key_size + 4) as usize;

        let key = to_string(&data[key_offset..value_offset])?;
        let value_size = (&data[value_offset - 4..value_offset]).read_u32::<BigEndian>()? as usize;
        let trailer_offset = value_offset + value_size + 8;
        let value = to_string(&data[value_offset..value_offset + value_size])?;
        let trailer_size =
            (&data[trailer_offset - 8..trailer_offset]).read_u64::<BigEndian>()? as usize;
        let trailer = to_string(&data[trailer_offset..trailer_offset + trailer_size])?;

        Ok(CtrlMsg {
            typ: ctrl_msg_type,
            key,
            value,
            trailer,
        })
    }

    pub fn encode(&self) -> Result<Vec<u8>, Error> {
        let msg_type = self.typ.to_be_bytes();

        let key_len = ((self.key.len() + 1) as u32).to_be_bytes();
        let value_len = ((self.value.len() + 1) as u32).to_be_bytes();
        let trailer_len = ((self.trailer.len() + 1) as u64).to_be_bytes();

        let key = CString::new(self.key.as_str())?;
        let value = CString::new(self.value.as_str())?;
        let trailer = CString::new(self.trailer.as_str())?;

        let mut message = vec![];
        message.extend_from_slice(&msg_type);

        message.extend_from_slice(&key_len);
        message.extend_from_slice(key.as_bytes_with_nul());

        message.extend_from_slice(&value_len);
        message.extend_from_slice(value.as_bytes_with_nul());

        message.extend_from_slice(&trailer_len);
        message.extend_from_slice(trailer.as_bytes_with_nul());

        let mut data = vec![];
        data.extend_from_slice(&(message.len() as u32).to_be_bytes());
        data.extend_from_slice(&message);

        Ok(data)
    }
}

fn to_string(data: &[u8]) -> Result<String, FromUtf8Error> {
    let it = data
        .iter()
        .enumerate()
        .filter(|(_, x)| **x == 0x0)
        .map(|(i, _)| i)
        .min()
        .unwrap_or_else(|| data.len());
    String::from_utf8(data[..it].to_vec())
}

fn pad_to_size(data: &[u8], size: usize) -> Vec<u8> {
    let mut data = data.to_vec();
    if size > data.len() {
        let extra = size - data.len();
        let padding = vec![0; extra];
        data.extend_from_slice(&padding);
    }
    data
}

#[cfg(test)]
mod tests {
    use crate::{CtrlMsg, Decoder, InitMsg, Message};
    use std::io::Read;

    const PAYLOAD_ONE: &[u8] = &[
        0x2d, 0x2d, 0x73, 0x70, 0x6c, 0x75, 0x6e, 0x6b, 0x2d, 0x63, 0x6f, 0x6f, 0x6b, 0x65, 0x64,
        0x2d, 0x6d, 0x6f, 0x64, 0x65, 0x2d, 0x76, 0x33, 0x2d, 0x2d, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x32, 0x39, 0x64, 0x31, 0x38, 0x39,
        0x66, 0x66, 0x64, 0x32, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x30, 0x38, 0x39, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ];

    const PAYLOAD_TWO: &[u8] = &[
        0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x13, 0x5f, 0x5f, 0x73,
        0x32, 0x73, 0x5f, 0x63, 0x61, 0x70, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x69, 0x65, 0x73,
        0x00, 0x00, 0x00, 0x00, 0x14, 0x61, 0x63, 0x6b, 0x3d, 0x30, 0x3b, 0x63, 0x6f, 0x6d, 0x70,
        0x72, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x3d, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x05, 0x5f, 0x72, 0x61, 0x77, 0x00,
    ];

    #[test]
    fn decode() {
        let expected_init = InitMsg {
            identifier: "--splunk-cooked-mode-v3--".to_string(),
            servername: "829d189ffd2b".to_string(),
            api_port: 8089,
        };

        let payload = PAYLOAD_ONE.chain(PAYLOAD_TWO);
        let mut decoder = Decoder::new(payload);
        let msg = decoder.decode();

        match msg {
            Ok(Message::InitMsg(actual)) => assert_eq!(expected_init, actual),
            e => panic!("InitMsg not decoded correctly: {:?}", e),
        }

        let expected_ctrl = CtrlMsg {
            typ: 1,
            key: "__s2s_capabilities".to_string(),
            value: "ack=0;compression=0".to_string(),
            trailer: "_raw".to_string(),
        };

        let msg = decoder.decode();
        match msg {
            Ok(Message::CtrlMsg(actual)) => assert_eq!(expected_ctrl, actual),
            _ => panic!("CtrlMsg not decoded correctly"),
        }
    }

    #[test]
    fn encode_init() {
        let expected = PAYLOAD_ONE;
        let init_msg = InitMsg {
            identifier: "--splunk-cooked-mode-v3--".to_string(),
            servername: "829d189ffd2b".to_string(),
            api_port: 8089,
        };

        let ret = init_msg.encode();

        match ret {
            Ok(actual) => assert_eq!(expected, actual.as_slice()),
            _ => panic!("Error encoding InitMsg"),
        }
    }

    #[test]
    fn encode_ctrl() {
        let expected = PAYLOAD_TWO;
        let ctrl = CtrlMsg {
            typ: 1,
            key: "__s2s_capabilities".to_string(),
            value: "ack=0;compression=0".to_string(),
            trailer: "_raw".to_string(),
        };

        match ctrl.encode() {
            Ok(actual) => assert_eq!(expected, actual.as_slice()),
            _ => panic!("Error encoding CtrlMsg"),
        }
    }
}
